console.log("Client-side code running");
const createFileForm = document.getElementById("createFileForm");
createFileForm.addEventListener("submit", createFileRequest);
const moveFileForm = document.getElementById("moveFileForm");
moveFileForm.addEventListener("submit", moveFileRequest);
const createFolderForm = document.getElementById("createFolderForm");
createFolderForm.addEventListener("submit", createFolderRequest);
const deleteFolderForm = document.getElementById("deleteFolderForm");
deleteFolderForm.addEventListener("submit", deleteFolderRequest);
const deleteFileForm = document.getElementById("deleteFileForm");
deleteFileForm.addEventListener("submit", deleteFileRequest);
const execCommandForm = document.getElementById("execCommandForm");
execCommandForm.addEventListener("submit", execCommandRequest);

async function createFileRequest(event) {
  const value = prepareRequest(event);
  console.log("Create file request");
  let response = await fetch("http://localhost:3001/addFile", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(value),
  });
  await getFolders();
  console.log(response.text());
}

async function moveFileRequest(event) {
  const value = prepareRequest(event);
  console.log("Move file request");
  let response = await fetch("http://localhost:3001/moveFile", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(value),
  });
  await getFolders();
  console.log(response.text());
}

async function createFolderRequest(event) {
  const value = prepareRequest(event);
  console.log("Create folder request");
  let response = await fetch("http://localhost:3001/addFolder", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(value),
  });
  await getFolders();
  console.log(response.text());
}

async function execCommandRequest(event) {
  const value = prepareRequest(event);
  console.log("Execute command request");
  let response = await fetch("http://localhost:3001/shell", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(value),
  });
  await getFolders();
  console.log(response.text());
}

async function deleteFolderRequest(event) {
  const value = prepareRequest(event);
  console.log("Delete folder request");
  let response = await fetch("http://localhost:3001/deleteFolder", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(value),
  });
  await getFolders();
  console.log(response.text());
}

async function deleteFileRequest(event) {
  const value = prepareRequest(event);
  console.log("Delete file request");
  let response = await fetch("http://localhost:3001/deleteFile", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(value),
  });
  await getFolders();
  console.log(response.text());
}

function prepareRequest(event) {
  event.preventDefault();
  const data = new FormData(event.target);
  const value = Object.fromEntries(data.entries());
  console.log({ value });
  return value;
}

async function getFolders() {
  const folder = "data";
  const raw = JSON.stringify({
    folder: folder,
  });

  let response = await fetch("http://localhost:3001/folder", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: raw,
  });
  let folders = await response.json();
  console.log(folders);
  console.log(folders.message);
  document.getElementById("folderArray").innerHTML = "";
  for (let i = 0; i < folders.message.length; i++) {
    document.getElementById("folderArray").innerHTML +=
      "<li>" + folders.message[i] + "</li>";
  }
}

getFolders();
