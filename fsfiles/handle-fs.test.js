const {
  listFolder,
  createFolder,
  createFile,
  deleteFolder,
  deleteFile,
  moveFolder,
  moveFile,
  shellExecFile,
} = require("./handle-fs");
const dataFolder = `${__dirname}/datatest`;

describe("test handle-fs methods", () => {
  describe("createFolder", () => {
    afterAll(() => {
      deleteFolder(dataFolder);
    });
    it("should return true when folder is created", () => {
      console.log(dataFolder);
      const result = createFolder(dataFolder);
      expect(result).toBe(true);
    });
    it("should throw an error when folder already exist", async () => {
      deleteFolder(dataFolder);
      createFolder(dataFolder);

      expect(() => {
        createFolder(dataFolder);
      }).toThrow();
    });
  });

  describe("listFolder", () => {
    afterEach(() => {
      try {
        deleteFolder(dataFolder);
      } catch (err) {}
    });
    it("should list files from a folder", () => {
      const output = ["test.txt"];

      createFolder(dataFolder);
      output.forEach((file) => createFile(dataFolder, file, "Hello world!"));
      const result = listFolder(dataFolder);

      expect(result).toEqual(output);
    });
    it("should throw an error when folder doesn't exist", () => {
      expect(() => {
        listFolder(dataFolder);
      }).toThrow();
    });
  });

  describe("deleteFolder", () => {
    it("should delete folder", () => {
      createFolder(dataFolder);
      const result = deleteFolder(dataFolder);
      expect(result).toBe(true);
    });
    it("should throw an error when folder doesn't exist", () => {
      expect(() => {
        deleteFolder(dataFolder);
      }).toThrow();
    });
  });

  describe("deleteFile", () => {
    afterEach(() => {
      try {
        deleteFolder(dataFolder);
      } catch (err) {}
    });
    it("should delete file", () => {
      createFolder(dataFolder);
      createFile(dataFolder, "test.txt", "Hello world!");
      const result = deleteFile(dataFolder, "test.txt");
      expect(result).toBe(true);
    });
    it("should throw an error when file doesn't exist", () => {
      expect(() => {
        deleteFile(dataFolder, "test.txt");
      }).toThrow();
    });
  });

  describe("moveFolder", () => {
    const secondRepo = `${__dirname}/repo`;
    it("should move folder", () => {
      expect(() => {
        createFolder(dataFolder);
        createFolder(secondRepo);
        const result = moveFolder(dataFolder, `${secondRepo}/datatest`);
        expect(result).toBe(true);
        deleteFolder(secondRepo);
      }).not.toThrow();
    });
    it("should throw an error when folder doesn't exist", () => {
      expect(() => {
        const result = moveFolder(dataFolder, `${secondRepo}/datatest`);
        expect(result).toBe(true);
        deleteFolder(secondRepo);
      }).toThrow();
    });
  });

  describe("moveFile", () => {
    const secondRepo = `${__dirname}/repo`;
    it("should move file then return true", () => {
      expect(() => {
        createFolder(dataFolder);
        createFolder(secondRepo);
        createFile(dataFolder, "test.txt", "Hello World");
        const result = moveFile(dataFolder, `${secondRepo}/datatest`);
        expect(result).toBe(true);
        deleteFolder(secondRepo);
      }).not.toThrow();
    });
    it("should throw an error when folder doesn't exist", () => {
      expect(() => {
        const result = moveFile(dataFolder, `${secondRepo}/datatest`);
        deleteFolder(secondRepo);
      }).toThrow();
    });
  });

  // describe("shellExecFile", () => {
  //   it("should move file then return true", () => {
  //     expect(() => {
  //       shellExecFile("echo 'Hello World'");
  //     }).not.toThrow();
  //   });
  // });
});
