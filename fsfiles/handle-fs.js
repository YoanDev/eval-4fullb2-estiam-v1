const fs = require("fs");
const { stdout, stderr } = require("process");
const readline = require("readline");
const exec = require("child_process").exec;

const dir = `${__dirname}/data`;

const createFolder = (folder) => {
  if (!fs.existsSync(folder)) {
    try {
      fs.mkdirSync(folder);
      return true;
    } catch (error) {
      throw new Error("Something went wrong, folder creation failed");
    }
  } else {
    throw new Error("Folder already exist");
  }
};

const createFile = (folder = dir, file, msg = "") => {
  const path = `${folder}/${file}`; // J'ai changé le / parce que je suis sur UNIX
  if (!fs.existsSync(path)) {
    fs.writeFileSync(path, msg);
    return true;
  } else {
    console.log(`${path} already exist`);
  }
};

const listFolder = (folder = dir) => {
  if (fs.existsSync(folder)) {
    return fs.readdirSync(folder);
  } else {
    throw new Error("Folder doesn't exist");
  }
};

const deleteFolder = (folder) => {
  if (fs.existsSync(folder)) {
    //rmdirSync est obsolète et retourne une erreur
    fs.rmSync(folder, { recursive: true, force: true });
    console.log(`Folder ${folder} successfully deleted`);
    return true;
  } else {
    throw new Error("Folder doesn't exist");
  }
};

const deleteFile = (folder, file) => {
  if (fs.existsSync(`${folder}/${file}`)) {
    fs.unlinkSync(`${folder}/${file}`);
    console.log(`File ${folder}/${file} successfully deleted`);
    return true;
  } else {
    throw new Error("File doesn't exist");
  }
};

const moveFolder = (oldPath, newPath) => {
  try {
    fs.renameSync(oldPath, newPath);
    console.log(`Folder moved from ${oldPath} to ${newPath}`);
    return true;
  } catch (error) {
    throw new Error("Move folder failed !");
  }
};

const moveFile = (oldPath, newPath) => {
  try {
    fs.renameSync(oldPath, newPath);
    console.log(`File moved from ${oldPath} to ${newPath}`);
    return true;
  } catch (error) {
    throw new Error("Move file failed !");
  }
};

const shellReq = async (file) => {
  const fileStream = fs.createReadStream(file);
  const arrayLines = [];

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });

  for await (const line of rl) {
    console.log(`Line from file: ${line}`);
    arrayLines.push(line);
  }
  return arrayLines;
};
const shellExecFile = (commandline) => {
  exec(commandline, (e, stdout, stdeer) => {
    if (e instanceof Error) {
      console.log(e);
      console.log(stdeer);
      throw e;
    }
    console.log(stdout);
    return true;
  });
};

module.exports.listFolder = listFolder;
module.exports.createFolder = createFolder;
module.exports.createFile = createFile;
module.exports.deleteFolder = deleteFolder;
module.exports.deleteFile = deleteFile;
module.exports.moveFolder = moveFolder;
module.exports.moveFile = moveFile;
module.exports.shellReq = shellReq;
module.exports.shellExecFile = shellExecFile;
