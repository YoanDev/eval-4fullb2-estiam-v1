const express = require("express");
const bodyParser = require("body-parser");
const {
  createFolder,
  createFile,
  deleteFolder,
  deleteFile,
  moveFile,
  shellExecFile,
} = require("./fsfiles/handle-fs");
const fs = require("fs");
const app = express();
const port = 3001;

app.use(express.static("public"));
app.use(bodyParser.json());

const dataFolder = `${__dirname}/data/`;
const fileToRead = `${__dirname}/data/fileToRead.txt`;

if (!fs.existsSync(dataFolder)) createFolder(dataFolder);
if (!fs.existsSync(fileToRead)) createFile(dataFolder, 'fileToRead.txt', 'Amine arrête de buguer');


// example for Grizzli :
// shellExecFile('dir')
shellExecFile('echo "Coucou !"')


app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.post("/addFile", (req, res) => {
  console.log(req.body);

  try {
    let r = createFile(dataFolder, req.body.filename, req.body.text);
    res.status(200).json({
      status: 200,
      message: "Request accepted",
      info: r,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      message: "Request failed",
      error: err,
    });
  }
});

app.post("/moveFile", (req, res) => {
  console.log(req.body);

  try {
    let r = moveFile(
      dataFolder + req.body.filename,
      dataFolder + req.body.newFilename
    );
    res.status(200).json({
      status: 200,
      message: "Request accepted",
      info: r,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      message: "Request failed",
      error: err,
    });
  }
});

app.post("/addFolder", (req, res) => {
  console.log(req.body);

  try {
    let r = createFolder(dataFolder + req.body.folderName);
    res.status(200).json({
      status: 200,
      message: "Request accepted",
      info: r,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      message: "Request failed",
      error: err,
    });
  }
});

app.post("/deleteFile", (req, res) => {
  console.log(req.body);

  try {
    let r = deleteFile(dataFolder, req.body.filename);
    res.status(200).json({
      status: 200,
      message: "Request accepted",
      info: r,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      message: "Request failed",
      error: err,
    });
  }
});

app.post("/deleteFolder", (req, res) => {
  console.log(req.body);

  try {
    let r = deleteFolder(dataFolder + (req.body.folderName || "trash"));
    res.status(200).json({
      status: 200,
      message: "Request accepted",
      info: r,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      message: "Request failed",
      error: err,
    });
  }
});

app.post("/folder", (req, res) => {
  const folder = req.body.folder || "data";
  console.log("Folder : " + folder);

  if (fs.existsSync(folder)) {
    console.log("Contains : " + fs.readdirSync(folder));
    res.status(200).json({
      status: 200,
      message: fs.readdirSync(folder),
    });
    return fs.readdirSync(folder);
  } else {
    throw new Error("Folder doesn't exist");
  }
});

app.post("/shell", async (req, res) => {
  console.log(req.body);

  try {
    let r = await shellExecFile(req.body.commande);
    res.status(200).json({
      status: 200,
      message: "Request accepted",
      info: r,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      message: "Request failed",
      error: err,
    });
  }
});

app.listen(port, () => {
  console.log("Server started at http://localhost:" + port);
});
