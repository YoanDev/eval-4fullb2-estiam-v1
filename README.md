Projet: Formation Fullstack – Back avancé

# Groupe :
- Yoan MENDES SEMEDO
- Alexandre BENALI
- Lory Stan TANASI CHAMSOUDINE
- Amine ABBES



# Comment fonctionne le projet ?
- Le fichier server.js contient le démarrage du serveur sur le port 3001, les endpoints, le déclenchement de la méthode de création du fichier data
- Le fichier handle-fs.js contient toutes les méthodes permettant la gestion de fichier
- Le fichier handle-fs.test.js contient tous les tests de méthodes dans deux cas, un cas de réussite et un cas d’erreur
- Dans le fichier index.html on retrouve ce qui est relatif à la vue, les formulaires permettant l’exécution des commandes
- Le fichier client.js contient les endpoints et les callbacks des submit de formulaire
- Le fichier style.css contient le css du front

- Le gitignore empeche la synchronisation du node_modules et du fichier data ainsi que son contenu d’être sur le repository distant(gitlab)








# Qui a fait quoi ?

                         Yoan
- Ajout de méthodes pour :
-	créer un dossier 
-	créer un fichier
-	effectuer une requête dans le terminal -Ajout de méthode pour lire un fichier ligne par ligne 
- mise en place de test unitaires 
- Ajout de css
- rédaction pdf


                    Alexandre

Création du front Formulaires pour : 
-  Création d'un dossier 
-  Création d'un fichier 
-  Suppression d'un dossier 
-  Suppression d'un fichier 
-  Déplacer un dossier/fichier 
-  Exécuter une commande Shell Requêtes HTTP vers le back



                         Stan
Création des fonctions :   
- suppression d'un dossier  
- suppression d'un fichier 
- déplacer un dossier 
- déplacer un fichier 
- ajout des tests unitaires pour tester ces fonctions.






                         Amine
Création du front Formulaires pour : 
- Création d'un dossier 
- Création d'un fichier 
- Suppression d'un dossier 
- Suppression d'un fichier 
- Déplacer un dossier/fichier 
